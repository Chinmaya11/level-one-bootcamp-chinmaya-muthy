//WAP to find the distance between two point using 4 functions.
#include <stdio.h>
#include<math.h>
float input(char b[])
{
    float a;
    printf("Enter the value of %s: ",b);
    scanf("%f",&a);
    return a;
}

float compute_dist(float x1,float y1,float x2,float y2 )
{
    float dist;
    dist=sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
    return dist;
}

void output(float distance)
{
    printf("Distance between two points: %f",distance);
}

int main()
{
    float x1,y1,x2,y2,distance;
    x1=input("x1");
    x2=input("x2");
    y1=input("y1");
    y2=input("y2");
    distance=compute_dist(x1,y1,x2,y2);
    output(distance);
    return 0;
}
